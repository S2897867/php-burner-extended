<?php
/**
 * [app description]
 * this class has been diesinged to:
 * --privide a router for post and get requests
 * --link routs to controllers functions and models
 * --store env settings under the env setting
 * --provide a single space for libaries to live under the libs var
 */
class app {
    private $routs = [];//lost all all routs
    private $libs = [];//list of libaries
    private $env = [];//list of server settings
   /**
    * [__construct description]
    * this is the function will start up the inital state
    * @param array $bootvars [
    * you can pass parameters to this function through the bootvars
    * if you set the booth path to anything else it must have a file
    *  path to the env as not specifying one will prevent the loading
    *  of the env file]
    */
    public function __construct ($bootvars = ['filePath' => './../.env' ]){
        $this->env = $bootvars;
        //start boot process
        $this->importEnv();//import env
        $this->routs['POST'] = []; //all post routs and there linked functions
        $this->routs['GET'] = []; //add get routs and there linked functions
        /*
            list of routs and there slected mode -ote that by calling a post and get with same name will overide eachother
        */
        $this->routs['typeList'] = [];

    }

   /**
    * [use description]
    * this function is designed to add a libary to that app name space
    * @param  [string] $libName [the name you want to store ]
    * @param  [type] $libary  [must be an object that you libary uses]
    * @return [null]          [description]
    */
    public function use ($libName,$libary){
        $this->libs[$libName] = $libary;
    }
    /**
     * [getLibs description]
     * gets all lobaries and returns a deep copy array
     * @return [array] [array of libaries in use]
     */
    public function getLibs(){
        return $this->libs;
    }

    /**
     * [getLib description]
     * this function gets a single libary with the name given
     * @param  [string] $libName [the name that the libary is stored under]
     * @return [obj]          [the instaciated libary]
     */
    public function getLib($libName){
        return $this->libs[$libName];
    }

    /**
     * [set description]
     * @param [type] $settingName [description]
     * @param [type] $libary      [description]
     */
    public function set ($settingName, $libary){
        $this->env[$settingName] = $libary;
    }
    /**
     * [importEnv description]
     * is a privet function that imports the env settings file
     * all framework settings must be added to the .env file
     * do not clutter up the name space with settings vars!
     * @return [null] [description]
     */
    private function importEnv(){
        $file = file_get_contents($this->env['filePath']);
        $env = json_decode($file,true);
        $this->env = $env;
    }

    /**
     * [getEnv description]
     * gets the entirity of the env settings and returns them as a array
     * @return [array] [array of settings]
     */
    public function getEnv(){
        return $this->env;
    }

    /**
     * [get description]
     * adds a get request to the app and links it via a rout to a function or class
     * @param  [string] $URI [URI as a srting]
     * @param  [function referance] $fn  [a function referance - must be static or public]
     * @return [null]      [description]
     */
    public function get($URI,$fn){
        $this->routs['GET'][$URI] = $fn;
        $this->routs['typeList'][$URI] ='GET';
    }
    
    /**
     * [post description]
     * adds a post request to the app and links it via a rout to a function or class
     * @param  [string] $URI [URI as a srting]
     * @param  [function referance] $fn  [a function referance - must be static or public]
     * @return [null]      [description]
     */
    public function post($URI,$fn){
        $this->routs['POST'][$URI] = $fn;
        $this->routs['typeList'][$URI] ='POST';
    }

    /**
     * [go description]
     * is used to scan the URI and return the right rout linked function
     * if a URI dose not link to a route than a 404 is returned
     * @param  [array] $req [the request var]
     * @param  [obj] $res [responce obj that contains show and render]
     * @param  [obj] $app [referance to app]
     * @return [null]      [description]
     */
    public function go($req,$res,$app){
        if (array_key_exists ($req["URI"],$this->routs['typeList'])){
            $type = $this->routs['typeList'][ $req ['URI'] ];
            if ($req['reqMode'] === $type){
                $fn = $this->routs[$type][ $req ['URI'] ];
                //echo "test";
                $fn($req,$res,$app);
            }else{
                echo "conection refused";
            }
        }else{
            echo "404";
            die;
        }
    }

/**
 * [useRenderEngine description]
 * sets the render engine and adds it to the responce class
 * @param  [obj] $res    [responce obj]
 * @param  [obj] $engine [referance to the render engine]
 * @return [null]         [description]
 */
    public function useRenderEngine ($res,$engine){
        $res->engine = $engine;
    }
}?>
