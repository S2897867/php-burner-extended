<?php
class POST {
    public static function  sanitizeAll($dontDoPostNameArray=array()){
        foreach (array_keys($_POST) as $name){
            if (!in_array($name,$dontDoPostNameArray)){
                $_POST[$name] = trim(strip_tags($_POST[$name]));
            }
            //console()->log($_POST[$name]);
        }
    }
    public static function sanitize ($postName, $leaveTags=""){
        if (isset($_POST[$postName])){
            $_POST[$postName] = trim(strip_tags($_POST[$postName], $leaveTags));
            return 1;
        }
        return 0;
    }
    public static function is_set ($postName){
        //if (isset($_POST[$postName]) && $_POST[$postName] != "" ){
        if (isset($_POST[$postName])){
            return 1;
        }
        return 0;
    }
    public static function input ($postName){
        if (isset($_POST[$postName])){
            return $_POST[$postName];
        }
        return null;
    }
    /*
        validators
    */
    public static function isNum($postName){
        if (POST::is_set($postName)){
            console()->log("is set");
            if (
                filter_var(POST::input($postName), FILTER_VALIDATE_INT) !== FALSE ||
                filter_var(POST::input($postName), FILTER_VALIDATE_FLOAT) !== FALSE
            ){
                console()->log("is num");
                return 1;
            }
        }
        return 0;
    }

    public static function isInt($postName){
        if (POST::is_set($postName)){
            console()->log("is set");
            if (filter_var(POST::input($postName), FILTER_VALIDATE_INT) !== FALSE){
                console()->log("is int");
                return 1;
            }
        }
        return 0;
    }

    public static function isFloat($postName){
        if (POST::is_set($postName)){
            console()->log("is set");
            if (filter_var(POST::input($postName), FILTER_VALIDATE_FLOAT) !== FALSE){
                console()->log("is float");
                return 1;
            }
        }
        return 0;
    }

    public static function isString($postName){
        if (POST::is_set($postName) && $_POST[$postName] != "" ){
            if (
                is_string($_POST[$postName]) && (
                    filter_var(POST::input($postName), FILTER_VALIDATE_INT) === FALSE ||
                    filter_var(POST::input($postName), FILTER_VALIDATE_FLOAT) === FALSE
                ) &&
                POST::isBool($postName) != 1
            ){
                return 1;
            }
        }
        return 0;
    }

    public static function isBool($postName){
        if (POST::is_set($postName)){
                if (
                    $_POST[$postName] === true ||
                    $_POST[$postName] === false ||
                    $_POST[$postName] === "true" ||
                    $_POST[$postName] === "false" ||
                    $_POST[$postName] === "1" ||
                    $_POST[$postName] === "0"
                    ){
                    return 1;
                }
        }
        return 0;
    }

    public static function isEmail($postName){
        if (POST::is_set($postName)){
            if (
                filter_var($_POST[$postName], FILTER_VALIDATE_EMAIL)
                ){
                return 1;
            }
        }
        return 0;
    }

    public static function isPhone($postName){
        if (POST::is_set($postName)){
            $casedNumber = Cast::phone($_POST[$postName]);
            if (strlen($casedNumber) >= 8 && strlen($casedNumber) <=15){
                return 1;
            }
        }
        return 0;
    }

}

?>
