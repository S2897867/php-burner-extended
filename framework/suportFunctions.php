<?php
$rootVar['suportFunctions.php'] = "Working";

function get ($get, $leaveTags=''){ //leave tags will leave any html tags you enter
    if (isset($_GET[$get]) && !empty($_GET[$get]))
        return trim(strip_tags($_GET[$get], $leaveTags));
    else
        return null;
}

function post ($post, $leaveTags=''){ //leave tags will leave any html tags you enter
    //var_dump($_POST[$post]);
    if (isset($_POST[$post]) && !empty($_POST[$post]))
        return trim(strip_tags($_POST[$post], $leaveTags));
    else
        return null;
}

function console (){
    return $GLOBALS['console'];
}

function view (){
    return $GLOBALS['view'];
}

function getFrameworkMode (){
    return $GLOBALS['frameWorkMode'];
}


/*
this function creates a new session
if a session has not been created
*/
function newSession (){
//if session is not started start the session
  if ( session_id() != ''){
    return false;
  }else{
    session_start();
    return true;
  }
}

/*
this function is responsable for
preventing issues where using
just session start on other pages after the
session has been started causes an error
this function fixes this
  */
function continueSession(){
  return newSession ();
}

/*
this function ends the session
*/
function endSession (){
  if ( session_id() != ''){
      session_destroy();
    return true;
  }else{
    return false;
  }
}

/*

*/

class Cast {
    public static function phone ($phoneNumber){
        $returnVal = trim(
                    preg_replace('/\D/', '',
                        preg_replace('~[-/ ]~', '', $phoneNumber)
                    )
                );
        if (strlen($phoneNumber) > 0 && trim($phoneNumber[0]) == '+'){
            $returnVal = '+'.$returnVal;
        }
        return $returnVal;
    }
}


?>
