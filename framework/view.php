<?php

class View{
    private $head = array(); //all the head elements
    private $bodyViews = array (); //all the body elements = the views
    public $title = ""; // the page title

    function __construct(){}
//-------------------------------------------
//this function sets up the head of the html documents
    public function _head (){
        $html = "
            <!DOCTYPE html>
            <html lang='en'>
                <head>
                    <meta charset='utf-8'>

        ";
        //include any thing that is for the dev console
        if (getFrameWorkMode()=='dev'){
            $html.="";
        }
        //include the libaries
        if ($GLOBALS['frameWork']["lib"]['jqury'] == true ){
            $html.= '<script type="text/javascript" src="lib/jquery-3.1.1.min.js"></script>';
        }
        if ($GLOBALS['frameWork']['lib']['bootStrap']== true ){
            //echo "bootStrap";
            $html.= '<link href="lib/bootstrap-3-3-7/css/bootstrap.min.css" rel="stylesheet">'.
            '<script src="lib/bootstrap-3-3-7/js/bootstrap.min.js"></script>';
        }
        if ($GLOBALS['frameWork']['lib']['fontAwesome'] == true){
            //echo "fontAwesome";
            $html.='<link rel="stylesheet" href="lib/font-awesome-4-6-3/css/font-awesome.min.css">';
        }
        if ($GLOBALS['frameWork']['lib']['materialize'] == true){
            //echo "materialize";
            $html.='<meta name="viewport" content="width=device-width, initial-scale=1.0"/>'.
            ' <link type="text/css" rel="stylesheet" href="lib/materialize/css/materialize.min.css"  media="screen,projection"/>'.
            '<script src="lib/materialize/js/materialize.min.js"></script>';

        }
        return $html;
    }
//this function set up the head close tag for the html
    public function head_ (){
        return "</head>";
    }
//this function gets all the head elements and returns them as a string
    function head (){
        $html="".$this->title;
        foreach ($this->head as $tag){
            $html.=$tag;
        }
        return $html;
    }
//this function gets starts the body of the html document
    public function _body (){
        return "<body>";
    }
//this function gets all the views
    public function body (){
        return '';
    }
//this function closes the body and html document
    public function body_ (){
        return "</body></html>";
    }
//adds the title
    public function title ($title){
        $this->title = "<title>".$title."</title>";
    }
//sets the lanuages
    public function lanuage ($lanuage="en"){}
//adds css file
    public function addCss($fileName){
        $this->head[] = "<link rel='stylesheet' type='text/css' href='css/$fileName'>";
    }
    public function addCssExt($filePath){
        $this->head[] = "<link rel='stylesheet' type='text/css' href='$filePath'>";
    }
//adds js
    public function addJs ($fileName){
        $this->head[] = "<script src='js/$fileName'></script>";
    }
    public function addJsExt ($filePath){
        $this->head[] = "<script src='$filePath'></script>";
    }
//adds a view to the list of views
    public function addView($fileName){
        $view = $this->phraseViewName($fileName);
        if ($this->viewExits($view)){//cheack if view file exists
            $this->bodyViews[] = $view;//add view if exits
        }else{
             //error view could not be found
        }
    }
//
    private function phraseViewName ($viewName){
        if (strpos($viewName, '.v.php') == 0 ){
            return $viewName.'.v.php';
        }else{
            return $viewName;
        }
    }
    private function viewExits ($view){
        if(is_file('../views/'.$view)){
            return true;
        }else{
            return false;
        }
}

//--------------------------------------------
    public function flush (){
         //$controllerVar = $GLOBALS['controllerVar'];
        extract( $GLOBALS, EXTR_PREFIX_SAME, "wddx");//passes all global vars into this function :)
        //var_dump($vars);
        echo $this->_head().$this->head().$this->title.$this->head_();
        echo $this->_body();
        foreach ($this->bodyViews as $bodyView){
            include_once('../views/'.$bodyView);
        }
            console()->show();
        echo $this->body_();
    }
}
?>
