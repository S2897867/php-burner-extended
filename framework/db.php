<?php
/*
File Writer Class V7
----------------------------------------------
This class and set of functions alows for conecting a data base.
All SQL entered into functions is sanitize.
----------------------------------------------
Note that the only suported database types are MYSQL and SQLite.
Note that you will have to set the database type after you have
created an instance of DatabaseConection.
*/
class DatabaseConection {
  private $db;
  /*
  This function sets up the object to
  conect to a SQLite database.
  $fileName passes a string of the database file
  location into the function.
  */
  public  function db_SQLITE($fileName){
    $fileName = strip_tags($fileName);
	    try {
		      $databse = new PDO("sqlite:"."../sqlite/".$fileName);
		      $databse->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $this->db=$databse;
          return true;
	     } catch (PDOException $e) /*error handling*/{
         console()->warning(
            "  SQLite databse could not be created because the file path
               '../framework/sqliteDB/$fileName'",
               "db_SQLITE('$fileName')",
               "DatabaseConection",
               $e
          );
        // //$GLOBALS['console']->addError ("db_SQLITE('$fileName')",
        //                                "
        //                               SQLite databse could not be created because the file path
        //                                '../framework/sqliteDB/$fileName'
        //                                 dose not exist.",$e);
        return false;
      }
  }

  /*
  This function sets up the object to
  conect to a MySQL database.
  */
public function db_MYSQL ($host,$username,$password,$dbname){
    try {
    $database = new PDO("mysql:host=$host;dbname=$dbname", $username, $password, array( PDO::ATTR_PERSISTENT => false));
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db=$database;
    return true;
    }catch (PDOException $e){
      console()->warning(
         " MySQL databse could not conected to $dbname.",
            "db_MYSQL('$host','$username','$password','$dbname')",
            "DatabaseConection",
            $e
       );
       return false;
    }
  }

    /*
    this function creats a new databse for MySQL
    */
    public function db_MYSQL_new ($host,$username,$password,$newDbname){
        try {
            $nDB = new PDO("mysql:host=$host", $username, $password);
            $nDB->exec("CREATE DATABASE `$newDbname`;".
                "GRANT ALL ON `$newDbname`.* TO '$username'@'localhost'; ".
                "FLUSH PRIVILEGES;"
            );
            return true;
        } catch (PDOException $e) {
            console()->warning(
               "MySQL databse could not create db $newDbname.",
                "db_MYSQL_new('$host','$username','$password','$newDbname')",
                "DatabaseConection",
                 $e
             );
        }
        return false;
    }

    /*
        this function drops a databse with MySQL
    */

    public function db_MYSQL_drop ($host,$username,$password,$dbname){
        try {
            $nDB = new PDO("mysql:host=$host", $username, $password);
            $nDB->exec("DROP DATABASE `$dbname`;");
            return true;
        } catch (PDOException $e) {
            console()->warning(
               "MySQL databse could not droped db $dbname.",
               "db_MYSQL_drop('$host','$username','$password','$dbname')",
               "DatabaseConection",
               $e
             );
            return false;
        }
    }

     /*
     The CRUD functions querie the data base.
     Note that any SQL passes is sanitised first

     $SQL is a string that contains the SQL
     $input is an array of values to be binded
     $bindTo is an array of values that $input gets binded to

     this function will return false if the data base is not
     set or if their was an error processing the SQL reuqest with the binded values
     */
      public function select ($SQL,$input=null,$bindTo=null){
       $db = $this->db;  //settings the database object in the class to a local varable
       if (isset ($this->db)){
           //find length of arrays
           $inputLength = count ($input);
           $bindToLength = count ($bindTo);
          //sanatisation for values
          $sanBind = array();
          $sanInput = array();
          if ($inputLength == $bindToLength && $bindToLength != 0 && $inputLength != 0){
               foreach ( $bindTo as $index ){
                 $sanBind[] = strip_tags($index);
               }
               foreach ( $input as $index ){
                 $sanInput[] = strip_tags($index);
               }
           }

           try{ //start accessing database
               $query=$SQL;
               $statement = $db->prepare($query);
               // bind values
               if ($inputLength == $bindToLength && $bindToLength != 0 && $inputLength != 0){
                   for ( $i = 0; $i < count($sanBind) && $i< count($input); $i++){
                       $statement->bindValue($sanBind[$i],$sanInput[$i]);
                   }
               }
               $statement->execute();
               $results = $statement->fetchAll();
               return $results;
             }catch (PDOException $e) {
                 console()->warning(
                    "DB unable to run SQL:",
                    "select('$SQL');",
                    "DatabaseConection",
                    $e);
               return 0;
             }
         }else{
             console()->warning(
                "DB not conected:",
                "select('$SQL');",
                "DatabaseConection");
                return 0;
         }
     }

     /*
       This function updates the database
     */
     public function update ($SQL,$input=null,$bindTo=null){
           $db = $this->db;  //settings the database object in the class to a local varable
           if (isset ($this->db)){
               //find length of arrays
               $inputLength = count ($input);
               $bindToLength = count ($bindTo);
              //sanatisation for values
              $sanBind = array();
              $sanInput = array();
              if ($inputLength == $bindToLength && $bindToLength != 0 && $inputLength != 0){
                   foreach ( $bindTo as $index ){
                     $sanBind[] = strip_tags($index);
                   }
                   foreach ( $input as $index ){
                     $sanInput[] = strip_tags($index);
                   }
               }

               try{ //start accessing database
                   $query=$SQL;
                   $statement = $db->prepare($query);
                   // bind values
                   if ($inputLength == $bindToLength && $bindToLength != 0 && $inputLength != 0){
                       for ( $i = 0; $i < count($sanBind) && $i< count($input); $i++){
                           $statement->bindValue($sanBind[$i],$sanInput[$i]);
                       }
                   }
                   $statement->execute();
                   //$results = $statement->fetchAll();
                   return 1;
                 }catch (PDOException $e) {
                     console()->warning(
                        "DB unable to run SQL:",
                        "CRUD('$SQL');",
                        "DatabaseConection",
                        $e);
                   return 0;
                 }
             }else{
                 console()->warning(
                    "DB not conected:",
                    "CRUD('$SQL');",
                    "DatabaseConection");
               return 0;
             }
         }

         public function insert ($SQL,$input=null,$bindTo=null){
             return $this->update ($SQL,$input,$bindTo);
         }
         public function delete ($SQL,$input=null,$bindTo=null){
             return $this->update ($SQL,$input,$bindTo);
         }
         public function drop ($SQL,$input=null,$bindTo=null){
             return $this->update ($SQL,$input,$bindTo);
         }
         public function create ($SQL,$input=null,$bindTo=null){
             return $this->update ($SQL,$input,$bindTo);
         }
  /*
  This function disconects the database.
  */
  public function db_disconnect(){
    $this->db = null;
  }

}//end class



//results accessing functions for accessing the the results array.
//legacy - no security.
function get_value($forEachAsVar,$collName){
  return $forEachAsVar[$collName];
}
//new - full security.
function get_value_strip_tags($forEachAsVar,$collName){
  //used in a foreach loop to get a value out of a results from the db_SQL function.
  return strip_tags($forEachAsVar[$collName]);
}


/*
This function opens a file get an SQL
comand and then returns the value in a string.
*/
function file_get_SQL ($fileName){
  try{
    return file_get_contents("modules/SQL/$fileName");
  }finally {
    return false;
  }

}





?>
