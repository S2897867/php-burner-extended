<?php
/*
    This file stores all the frameworks settings and variable initialisations
*/
/*
    The following settings is what mode you want to run the framework in.
    -set it to 'dev' and  PHP Burner will display the console.
    -set to 'deploy' and PHP Burner will not display the console or any php erros and warnings
*/
    $frameWorkMode = 'dev';

/*
    The flollowing varaibles tunr loading of css and js libaries on and off
*/
    $GLOBALS['frameWork'] = array();
    $GLOBALS['frameWork']["lib"] =  array ();
    $GLOBALS['frameWork']["lib"]['jqury'] = true;
    $GLOBALS['frameWork']["lib"]['bootStrap'] = false;
    $GLOBALS['frameWork']["lib"]['fontAwesome'] = true;
    $GLOBALS['frameWork']["lib"]['materialize'] = true;


/*
    The following variables set up the rest of the frame work
*/
    $view = new View();

/*
    The following code turns deve and deploy mode on and off
*/
    switch ($frameWorkMode){
        case 'dev':
            $console = new Console();
            break;
        case 'deploy':
            $console = new ConsoleEmpty();
            break;
        defoult:
            $console = new Console();
            break;
    }

    /*
        setUp the crf tokens
    */
    continueSession(); //continue the session
    if (!CRF::is_set()){//create the session if it dosent exist
        CRF::create();
    }
    require_once ("../helper.h.php");

?>
