<?php
class formInput {
    public $type;
    public $name;
    public $id;
    public $class;
    public $atributes = array ();
    public $safeTags = '';
    public $returnType;

    public $preHTML = '';
    public $postHTML = '';
}

class form {
    private $inputs = array (); //list of all the inputs
    private $inputOrder = array (); //list of order the inputs go in
    private $formOpen = '';
    private $formId = '';
    public  $lasterAddedInput;



    //
    function __construct($action,$id=''){
        //<button type='submit' form='form1' value='Submit'>Submit</button>
        //<form action="demo_form.asp" method="get" id="form1">
        $this->formOpen = "<form action='$action' method='post' id='$id'>";

    }

    public function addInput ($type,$name,$id='',$class=''){
        $this->lasterAddedInput = $name;
        $input = new formInput();
            $input->type = $type;
            $input->name = $name;
            $input->id = $id;
            $input->class = $class;
        $this->inputs[$name] = $input; //add input to list
        $this->inputOrder[] = $name; //record the oder the input were added
    }

    public function preHTML($html){
        $this->inputs[$this->lasterAddedInput]->preHTML = $html;

    }
    public function postHTML($html){
        $this->inputs[$this->lasterAddedInput]->postHTML = $html;

    }

    public function readonly (){
        $this->inputs[$this->lasterAddedInput]->atributes['readonly']='readonly';
    }
    public function value($inputValue){
        $this->inputs[$this->lasterAddedInput]->atributes['value']=$inputValue;
    }
    public function placeholder ($inputPlaceholder){
        $this->inputs[$this->lasterAddedInput]->atributes['placeholder']=$inputPlaceholder;

    }
    public function required (){
        $this->inputs[$this->lasterAddedInput]->atributes['required']='required';

    }

    public function show ($submitBtnName=false){
        $html = $this->formOpen;
        foreach ($this->inputOrder as $position){
            $input = $this->inputs[$position];

            $inputAtributes = $this->inputs[$position]->atributes;//the key and atribute sets
            $inputAtributeNames = array_keys($inputAtributes);//the key names in use
            var_dump($inputAtributeNames);
            $html.= $input->preHTML."<input type='".$input->type."'".
                    " name='$input->name'".
                    " id='$input->id'".
                    " class='$input->class'";
                    foreach ($inputAtributeNames as $atribute){
                            $html.=" $atribute='$inputAtributes[$atribute]' ";
                    }
                    $html.=" >".$input->postHTML;
        }
        if ($submitBtnName==false){
            //add submit buttons
            $html.="<button type='submit value='Submit'>Submit</button>";
        }elseif ($submitBtnName == null){
            //don't add button
        }else{
            $html.="<button type='submit value='Submit'>$submitBtnName</button>";
        }
        $html.='</form>';
        echo $html;
    }

    public function safeTags ($safeTags){
        $this->inputs[$this->lasterAddedInput]->safeTags=$safeTags;
//$input->safeTags
    }


    private function phrase (){
    }

    public function read(){
        if ($_SERVER["REQUEST_METHOD"] == "POST"){//change to POST in final build - will return true with GET as all urls are get
            $postVars = array();//array to store all post vars from form
            foreach ($this->inputs as $input){//find all post vars
                //var_dump($input);
                $postVars[$input->name] = post($input->name,$input->safeTags);
            }
            return $postVars; //return all post vars sanatised
        }else{
            //error to console
            return null;
        }
    }
}
?>
