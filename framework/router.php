<?php
    $rootVar['router.php'] = "Working";
    class router {
        private $controllerFileExtention = '.c.php';
        private $bindList = array ();
        function __construct (){}

        public function loadController (){
            extract( $GLOBALS, EXTR_PREFIX_SAME, "wddx");//passes all global vars into this function :)
            $route = get('route');
            //var_dump($route);
            //if their is no route than load the index page
            if ($route == null || $route == 'index' || $route == 'index.c.php'){
                require_once('../controllers/index.c.php');
                return;
            }
            //cheack bounded routs
            if (array_key_exists($route,$this->bindList)){
                //$this->bindList['$route'];
                if ($this->ifControllerFileExits($this->bindList[$route])){
                    $this->includeController($this->bindList[$route]);
                    return;
                }else{
                    //throw error
                    return;
                }
            }
            //continue on with regular routing
            //test if users has entered .c.php or not as route string
            if (strpos ($route, $this->controllerFileExtention) == 0){
                $route.= $this->controllerFileExtention;
                //var_dump($route." file");
            }
            //lookfor controller file and load it if it exits
            if ($this->ifControllerFileExits($route)){
                $this->includeController($route);
                //var_dump("found file");
                return;
            }

            //404 error - the page could not be found
            echo "404 page not found";
        }
        private function includeController ($route){
            require_once('../controllers/'.$route);
        }

        private function ifControllerFileExits ($fileName){
            if (file_exists($this->getFilePath($fileName))){
                return true;
            }else{
                return false;
            }
        }

        private function getFilePath ($controllerFile = ''){
            return dirname(__FILE__).'../controllers/'.$controllerFile;
        }

        public function addRoute ($route, $controllerFileName){
            $this->bindList[$route] = $controllerFileName;
            //var_dump($this->bindList);
        }

    }

?>
