<?php
/*
CRF Class V2
----------------------------------------------
This class is set up to help with cross side scripting.
It gives to two filed to echo into a form. These values
can be checked on the forms process page. We Recomend
you use this class only with ajax requests.
----------------------------------------------
*/


class CRF {
    public $tokenList; //2D array - AA
    public $viewList; //2D array - AA
    public $maxLength;
    public function __construct ($length = 10){
        $this->maxLength = $length;
        $this->tokenList = array();
        $this->viewList = array();
    }
    public function checkToken (){
        $return = 0;
        for ($i = 0; $i < count($this->tokenList); $i++){
            if (
                POST::input("crfToken") == $this->tokenList[$i] &&
                POST::input("crfTokenKey") == $this->viewList[$i]
                ){
                return 1;
            }
        }
        return 0;
    }

    public function removeToken (){

        $return = 0;
        for ($i = 0; $i < count($this->tokenList); $i++){
            if (
                POST::input("crfToken") == $this->tokenList[$i] &&
                POST::input('crfTokenKey') == $this->viewList[$i]
                ){
                    array_splice($this->tokenList,$i,1);
                    array_splice($this->viewList,$i,1);
                    return 1;
            }
        }
        return 0;

    }


    public function token (){
        $token = md5(uniqid(rand(), TRUE));
        $tokenpage = get('route');
        if ($tokenpage == null){
            $tokenpage = "index";
        }
        array_push($this->tokenList, $token);
        array_push($this->viewList, $tokenpage);
        if (count($this->tokenList) > $this->maxLength ){
            array_shift($this->tokenList);
            array_shift($this->viewList);
        }
        echo "<input type='text' name='crfToken' id='crfToken' value='$token' style='display:none;' readonly/>";
        echo "<input type='text' name='crfTokenKey' id='crfTokenKey' value='$tokenpage' style='display:none;' readonly/>";
    }

    /*
        This function is a wrapper for the cheack token function.
        It returns 1 if their if the token is found.
    */
    public static function check (){
        return $_SESSION["crf"]->checkToken();
    }

    /*
        This function is a wrapper for the classses constructor.
        It creates a new object and stores it in the session
    */
    public static function create (){
        $_SESSION["crf"] = new CRF();
    }
    /*
        This function cheacks to see if the session varable is set.
        Returns 1 on success.
    */
    public static function is_set (){
        if (isset($_SESSION["crf"])){
            return 1;
        }else{
            return 0;
        }
    }

    /*
        This function is a wrapper for the token function.
        It echos the HTML for the form.
    */
    public static function echoToken (){
        echo $_SESSION["crf"]->token();
    }

    /*
        This function is a wrapper for the remove token function.
        It removes token that has been submitted. You should cheak
        if the token is set and that it exists before calling this
        function.
    */
    public static function expireToken(){
        $_SESSION["crf"]->removeToken();
    }

}

 ?>
