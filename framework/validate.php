<?php
class Validate{
    public static function POST ($instructions = array()){
        $errors = array(
            'passed' => true,
        );
        foreach (array_keys($_POST) as $key){
            $errors [$key] = array();
        }
        foreach($instructions as $instruction){
            //console()->dump($instruction);
            if ($instruction ['required'] == true && POST::is_set($instruction['postName']) && $_POST[$instruction['postName']] != ""){ // required and set
                $errors = self::postHelper($instruction,$errors);

            }else if ($instruction['required'] == false && POST::is_set($instruction['postName']) && $_POST[$instruction['postName']] != ""){ //not required but set
                $errors =  self::postHelper($instruction,$errors);

            }else if ($instruction['required'] == true && (!POST::is_set($instruction['postName']) == 0 || POST::input($instruction['postName']) == "")){ //required and not set
                $errors[$instruction['postName']][]="This field is required.";
                $errors['passed']=false;
            }
        }
        //console()->dump($instructions);
        //console()->dump($errors);
        return $errors;

    }
    private static function postHelper($instruction,$errors){
        switch ($instruction['type']){
            case "string" :
                if (!POST::isString($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }else{
                    if ($instruction['max'] != false && strlen($instruction['old']) > $instruction['max']){
                        $errors[$instruction['postName']][]="Input is too long.";
                        $errors['passed']=false;
                    }
                    if ($instruction['min'] != false && strlen($instruction['old']) < $instruction['min']){
                        $errors[$instruction['postName']][]="Input is too short.";
                        $errors['passed']=false;
                    }
                }
                break;
            case "num" :
                if (!POST::isNum($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }else{
                    if ($instruction['max'] != false && $instruction['old'] > $instruction['max']){
                        $errors[$instruction['postName']][]="Input is too big.";
                        $errors['passed']=false;
                    }
                    if ($instruction['min'] != false && $instruction['old'] < $instruction['min']){
                        $errors[$instruction['postName']][]="Input is too small.";
                        $errors['passed']=false;
                    }
                }
                break;
            case "int" :
                if (!POST::isInt($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }else{
                    if ($instruction['max'] != false && $instruction['old'] > $instruction['max']){
                        $errors[$instruction['postName']][]="Input is too big.";
                        $errors['passed']=false;
                    }
                    if ($instruction['min'] != false && $instruction['old'] < $instruction['min']){
                        $errors[$instruction['postName']][]="Input is too small.";
                        $errors['passed']=false;
                    }
                }
                break;
            case "float" :
                if (!POST::isFloat($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }else{
                    if ($instruction['max'] != false && $instruction['old'] > $instruction['max']){
                        $errors[$instruction['postName']][]="Input is too big.";
                        $errors['passed']=false;
                    }
                    if ($instruction['min'] != false && $instruction['old'] < $instruction['min']){
                        $errors[$instruction['postName']][]="Input is too small.";
                        $errors['passed']=false;
                    }
                }
                break;
            case "bool" :
                if (!POST::isBool($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }
            break;
            case "phone" :
                if (!POST::isPhone($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }
                break;
            case "email" :
                if (!POST::isEmail($instruction['postName'])){ //string
                    $errors[$instruction['postName']][]="Format Error.";
                    $errors['passed']=false;
                }else{
                    if ($instruction['max'] != false && strlen($instruction['old']) > $instruction['max']){
                        $errors[$instruction['postName']][]="Input is too big.";
                        $errors['passed']=false;
                    }
                    if ($instruction['min'] != false && strlen($instruction['old']) < $instruction['min']){
                        $errors[$instruction['postName']][]="Input is too small.";
                        $errors['passed']=false;
                    }
                }
                break;
        }
        return $errors;
    }

    public static function InputPOST ($postName,$restrictionsInputString){
        $tempRestrictions = explode('|',$restrictionsInputString);
        $instructions = array (
            'postName' => $postName,
            'old' => POST::input($postName),
            'required' => false,
            'min' => false,
            'max' => false,
            'type' => false
        );
        foreach ($tempRestrictions as $item){
            if ($item == '*'){
                $instructions['required'] = true;
            }
            if ($item[0] == 'm' ){
                $instructions['min'] = str_replace('m',"",$item);
            }
            if ($item[0] == 'M' ){
                $instructions['max'] = str_replace('M',"",$item);
            }
            switch ($item){
                case "string" : $instructions['type'] = "string";  break;
                case "num" : $instructions['type'] = "num"; break;
                case "int" : $instructions['type'] = "int"; break;
                case "float" : $instructions['type'] = "float"; break;
                case "bool" : $instructions['type'] = "bool"; break;
                case "phone" : $instructions['type'] = "phone"; break;
                case "email" : $instructions['type'] = "email"; break;

            }
        }
        //console()->dump($tempRestrictions);
        //console()->dump($instructions);
        return $instructions;
    }

}
?>
