<?php 
/*
Email <-Plug-in-> V1
----------------------------------------------
This Class is responsible for
sending emails from a STAMP server through PHP
----------------------------------------------
*/

class Email {
   private $recipients = array ();
   private $subject = " ";
   private $message = " ";
   private $header = array ();
   //private $replyToEmail = "";


    /*
    this function adds recipants to the email.
    use multiple times to add more than one recipient.
    */
    public function addRecipient ($recipient){
        $this->recipients[] = $recipient;
    }
    
    /*
    this function adds a subject to the email
    */
    public function setSubject ($subject){
        $this->subject = $subject;
    }
    
    /*
    this function adds the message to the email
    */
    public function setMessage ($message){
        $this->message = $message;
    }
    
    /*
    this function sets the email so that html 
    content will be displayed corectly.
    only use use this function if you
    message contains html tags.
    
    */
    public function headSetHTML (){
      //$this->header[]  = 'MIME-Version: 1.0' . "\r\n";
      $this->header[]  = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    }
    
    
    /*
    this function adds the from header to the email
    */
    public function headSetFrom ($email){
        //$this->header[] = 'From: '.$email.'' . "\r\n";
        //$this->replyToEmail = $email;
    }

    public function headSetReplyEmail ($email){
      $this->header[] = 'Reply-To: '.$email.''. "\r\n";
    }
    
    
    /*
    this function takes all the detail about
    the email and then seds an email.
    */
    public function sendEmail (){
        $toString = "";
        //cheack emails are actual addresses
        foreach ($this->recipients as $email){
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                //error state
            }else{
                //add email to string
                $toString.= $email.', '; 
            }
        }
        
        //put all headers into one string 
        $headerString = "";
        foreach ($this->header as $head){
            $headerString.= $head;
        }
        
        
        
        mail($toString, $this->subject, $this->message, $headerString);
        //echo $headerString;
    }
}



?>