<?php
/*
File Manager Class V8
----------------------------------------------
This Class is responsible for
managing a apachie file systm.
Note that for best preformance when using
this class as an object you should store
it in a session varable
*/
class FileManager {
  public $folderPath = array();
  public $rootPath = "upLoads";

  //this function sets the root folder.
  //file access can't go above the rot folder
  public function __construct($rootPath=null){ //set defoult values
    if ($rootPath!= null){
      $this->rootPath = strip_tags($rootPath);
    }
  }

  //this function produces a string
  //based on the folder names stored
  public function get_location(){
    $folderPathSTR=$this->rootPath."/";
    foreach ($this->folderPath as $item){
      $folderPathSTR=$folderPathSTR.$item."/";
    }
    return $folderPathSTR;
  }

  //this function adds a folder
  public function folder_open ($newFolderName){
    $newFolderName = strip_tags($newFolderName);
    $dir = $this->get_location().$newFolderName."/";//if is a directory
     if (is_dir($dir)){
        $this->folderPath[] = $newFolderName;
        return true;
     }else /*error handling*/{
       console()->warning(
          "  The '$dir' location could not be found.",
             "folder_open('$newFolderName');",
             "FileManager"
        );
        return false;//end error handling by returning false
     }
  }

  //this function goes up a directory
  public function folder_up (){
    array_pop($this->folderPath);
  }

  //This function will get sub folder names with in the current location.
  public function get_folders (){//gets both files and folders
    $dir = $this->get_location();
    $dh  = opendir($dir);
    $files= array();

      if (is_dir($dir)){
        while (($file = readdir($dh)) !== false){//read everything that is in the folder
            if (strpos($file, ".") === false){ //remove files and stray characters
              $files[]=$file;
            }
        }
      }else /*error handling*/{
        $GLOBALS['console']->addError ("get_contence_folders()",
                                       "Can't read sub folders in the directory as '$dir' doesn't exist.");
        return false;//end error handling by returning false
      }
    closedir($dh);//garabage colection
    return $files;
  }

  //This function will get file names with in the current location.
  public function get_files (){//gets both files and folders
    $dir = $this->get_location();
    $dh  = opendir($dir);
    $files= array();

      if (is_dir($dir)){
        while (($file = readdir($dh)) !== false){//read everything that is in the folder
            if ($file != '.' && $file != '..' && $file != '...'){  //colect garbage
              if (strstr ($file, '.')){ //remove folders strstr() finds first instance of string - in this case we are looking for the '.' before the file extendtion.
                $files[]=$file;
              }
            }
        }
      }else /*error handling*/{
        $GLOBALS['console']->addError ("get_files()",
                                       "Can't read files in the directory as '$dir' doesn't exist.");
        return false;//end error handling by returning false
      }
    closedir($dh);//garabage colection
    return $files;
  }

  //This function uploads a file to the curent location
  public function upload_file (){
    //$target_dir = dirname(__FILE__)."/".$this->get_location();
    $target_dir = dirname(__FILE__)."/../../www/".$this->get_location();
    /* how the above code works
    dirname(__FILE__) -gets the folder this class is in
    /../../www/".$this->get_location(); navigates from teh above folder to the uploads folder
    */
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $GLOBALS['console']->addError ("File Upload Sucessful.",null,null);
        return true;
    }else /*error handling*/{
        $GLOBALS['console']->addError ("upload_file()",
                                       "Their was a problem uploading the file. '".
                                       basename( $_FILES["fileToUpload"]["name"])."'<br>to<br>".$target_dir);
        return false;//end error handling by returning false
    }
  }

  //This function will Create a folder with in the current location
  public function create_folder ($folderName){//creates folder
    $temp = $this->findNextFreeFolderName($folderName);
    //echo $temp;
    mkdir($this->get_location().$temp, 0777, true);
    return $temp;//addition return folder name - importnat if folder name has been changed
  }

  //this function will dleete a folder
  public function delete_folder ($folderName, $removeSub=false){
    $operationSuccess = false;
    if (!$removeSub){ //don't dlete sub folders and files
    $this->deleteFolderWithNoSub($folderName);
    }else{ //remove sub folders and files
      $this->deleteFolderAndSub($folderName);
    }
    if ($operationSuccess){
      return true;
    }//else return false and through error
     $GLOBALS['console']->addError ("delete_folder('$folderName',$removeSub)",
                                       "Their was a problem removing the file.
                                       ^See messages above for details ^");
      return false;
  }

  //this function dleetes files
  public function delete_file ($fileLocation){
    $fileLocation = strip_tags($fileLocation);
    if (file_exists($this->get_location().$fileLocation)){
      unlink($this->get_location().$fileLocation);
      return true;
    }else /*error handling*/{
        $GLOBALS['console']->addError ("deleete_file('$fileLocation');",
                                       "Location '".$this->get_location().$fileLocation."' doesn't exist.");
        return false;//end error handling by returning false
     }
  }

  //this function creates a new empty file
  public function new_file($newFileName){
    //creates file if non is their matching the same name
    $location = $this->get_location().strip_tags($newFileName);
      if (file_exists($location)==false){ //cheack to see if file is allready their
        //create file
         $file =fopen($location, 'w');
         fclose($file);
        return true;
      }else/*error handling*/{
        console()->warning(
            "  Can't create new file '$newFileName'.
            File at location '$location' already exists.",
            "new_file('$newFileName')",
            "FileManager"
        );
        return false;
      }
  }

  //this function reads a files contence - returns an array filed with each line of the file
  public function read_file ($fileName){
    $fileLOcation = $this->get_location().$fileName;
    if (file_exists($fileLOcation)){
      $contence = [];
      $file = fopen($fileLOcation, "r");
        while (($line = fgets($file)) !== false) {
        // process the line read.
        //removes new line characters - when witten back to the file new line characters are added
           $contence[] =  str_replace("\n", '', $line);

          //echo $line;
        }
      return $contence;
    }else{
      return null;
    }
  }

  //this function coppys a new file to a location
  public function copy_file ($fileName, $folderLocation, $newFileName){
    //cheack the file and new folder exists
    $existingFile = $this->get_location().$fileName;
    $newFile = $folderLocation.$newFileName;
    if (file_exists($existingFile)){
        if (is_dir($folderLocation)){
              //echo "true";
              //try to copy
              if (copy($existingFile,$newFile)){//copy file
                  return true;
              }
              //throw error if can't copy - this is a general error
               $GLOBALS['console']->addError ("copy_file ('$fileName', '$folderLocation', $newFileName')",
                                          "There was an error with coppying the file $fileName
                                           To $folderLocation. $newFileName.");
              return false;
        }else{
          //throw error if the copy to location dose not exist
           $GLOBALS['console']->addError ("copy_file ('$fileName', '$folderLocation', $newFileName')",
                                          "Can't copy file as Folder $folderLocation dose not exist.");
           return false;
        }
    }
    //throw error if the file to copy dose not exist
     $GLOBALS['console']->addError ("copy_file ('$fileName', '$folderLocation', $newFileName')",
                                          "Could not find the file $fileName in the directory $folderLocation.");
    return false;

  }

 //appends to end of file
  //this function writes a new line to a file
  public function apend_line_to_file ($fileName, $newline){
    $fileLOcation = $this->get_location().$fileName;
    if (file_exists($fileLOcation)){
      //get all curent lines
      $lines = [];
      $lines = $this->read_file ($fileName);
      //add new line
      $newLines = $lines;
      $newLines[] = $newline;
      //create string for writing
      $stringToWrite = "";
      foreach ($newLines as $line){
        //echo $line."<br>"; //test point 1
        $old = $stringToWrite;
        $stringToWrite = $old.$line."\n";

      }
      //write to file
      try{
        //echo $stringToWrite; //test point 2
        $file = fopen("$fileLOcation","w");
        fwrite($file, "$stringToWrite");
        sleep(1);
        fclose($file);
      }catch (Exception $e){
        //error opning the conection to the file/writing to file/closing to file
        $GLOBALS['console']->addError ("apend_lines_to_file('$fileName','$newlinesArray')",
                                       "Their was a problem writing to file. See PHP ERROR",$e);

        return false;
      }
    }else{
      //handle error
       $GLOBALS['console']->addError ("apend_lines_to_file('$fileName','$newlinesArray')",
                                       "Could not write to file as the file '$fileName' dose not exist in the directory '". $this->get_location()."'");

      return false;
    }
    return true;
  }

  // this function writes new an array to to a file
  public function apend_lines_to_file ($fileName,$newlinesArray){
    $fileLOcation = $this->get_location().$fileName;
    if (file_exists($fileLOcation)){
      //get all curent lines
      $lines = [];
      $lines = $this->read_file ($fileName);
      //add new lines from array to the lines to add array
      $newLines = $lines;
      foreach($newlinesArray as $newline){
         $newLines[] = $newline;
      }
      //create string for writing
      $stringToWrite = "";
      foreach ($newLines as $line){
        //echo $line."<br>";//test point 1
        $old = $stringToWrite;
        $stringToWrite = $old.$line."\n";

      }
      //echo $stringToWrite;//test point 2
      //write to file
      try{
        $file = fopen("$fileLOcation","w");
        fwrite($file, "$stringToWrite");
        sleep(1);
        fclose($file);
      }catch (Exception $e){
        //error opning the conection to the file/writing to file/closing to file
        $GLOBALS['console']->addError ("apend_lines_to_file('$fileName','$newlinesArray')",
                                       "Their was a problem writing to file. See PHP ERROR",$e);

        return false;
      }
    }else{
      //handle error
       $GLOBALS['console']->addError ("apend_lines_to_file('$fileName','$newlinesArray')",
                                       "Could not write to file as the file '$fileName' dose not exist in the directory '". $this->get_location()."'");

      return false;
    }
    return true;
  }

  //this function empties a file of all its contence
  public function clear_file ($fileName){
    $fileLOcation = $this->get_location().$fileName;
    if (file_exists($fileLOcation)){
      try{
        $file = fopen("$fileLOcation","w");
        fwrite($file, "");
        sleep(1);
        fclose($file);
      }catch (Exception $e){
        //error opning the conection to the file/writing to file/closing to file
        $GLOBALS['console']->addError ("clear_file('$fileName')",
                                       "Their was a problem writing to file. See PHP ERROR",$e);

        return false;
      }
    }else{
      //handle error
       $GLOBALS['console']->addError ("clear_file('$fileName')",
                                       "Could not write to file as the file '$fileName' dose not exist in the directory '". $this->get_location()."'");

      return false;
    }
    return true;

  }

  //thid function removes a folder add all that is with in it (folder and file)
  //this function is recursive - can handle up to 256 internal folders
  public function delete_folder_contents(){
    //down
    //find all files in folder and remove them
    $files = $this->get_files();
    foreach ($files as $file){
      //echo $file."<br>";
      $this->deleete_file($file);
    }
    //find child folders and repeete the process
    $folders = $this->get_folders();
    foreach ($folders as $folder){
      $this->folder_open($folder);
      $this->delete_folder_contents(); //recurse
      //up
      $this->folder_up();
      $this->delete_folder($folder);
    }

    return true;
  }

  //this function copys a folder and all of its contence
  //$fileManager is a varable that holds a seperate file manager which is set to where you want to copy to
  //copying from this to new file manager
  public function copy_folder ($folderName, $fileManager){
    //create new directory
    $folderName=$fileManager->create_folder($folderName);

    $fileManager->folder_open($folderName);
    //copy files into folder
    $files=$this->get_files();
    foreach ($files as $file){
      $this->copy_file ($file, $fileManager->get_location(), $file);
    }
    //get all folders and repeate
    $folders = $this->get_folders();
    foreach ($folders as $folder){
      $this->folder_open($folder);
      $this->copy_folder($folder,$fileManager);
      $this->folder_up();
    }

  }
  //------------------------------------------
  //helper functions
  //------------------------------------------

  //This function is a  helper for create folder
  //this function looks to see if a folder all ready exists and if so
  //find a new folder name to use
  private function findNextFreeFolderName ($folderName, $intger=1){
     if (is_dir($this->get_location().$folderName)){//if exists
       if (!is_dir($this->get_location().$folderName."_copy(".$intger.")")){//if new location dosent exists
        return $folderName."_copy(".$intger.")";
       }else{//try new location
         $intger++;
          return $this->findNextFreeFolderName($folderName,$intger);
       }
     }else{
       return $folderName;
     }




    /*
    if (is_dir($this->get_location().$folderName)){//if exists
      $intger++;//add 1 to folder name
      return $this->findNextFreeFolderName($folderName.$intger,$intger);//cheack to see if nex name exists
    }else{
      return $folderName;
    }
    */
  }

  //helper function for dleeting folder that has nothing it it
  private function deleteFolderWithNoSub ($folderName){
    $folderName = strip_tags($folderName);
    $dir = $this->get_location().$folderName."/";//if is a directory
    if (is_dir($dir)){ // if directory exists
      $this->folder_open($folderName); //get mesurement for next if statment
      if (count ($this->get_files()) == 0  && count ($this->get_folders()) == 0){
        //remove folder
        $this->folder_up();//return novagation to the corect place
        rmdir ($this->get_location().$folderName);
        return true;
      }else /*error handling*/{
        $this->folder_up();//return novagation to the corect place
        $GLOBALS['console']->addError ("delete_folder('$folderName');",
                                       "Their are sub files and folders in '$dir'. If you want to remove this folder use delete_folder('$folderName',true);");
        return false;//end error handling by returning false
      }
    }else /*error handling*/{
        $GLOBALS['console']->addError ("delete_folder('$folderName');",
                                       "Location '$dir' doesn't exist.");
        return false;//end error handling by returning false
    }
  }
  //helper function for dleeting folder with items in it
  private function deleteFolderAndSub ($folderName){
    $directory = $this->get_location().$folderName;
    //echo $directory;
    if (is_dir($directory)){
      $this->folder_open($folderName);
      $this->delete_folder_contents();
      $this->folder_up();
      $this->deleteFolderWithNoSub($folderName);
      return true;
    }else /*error handling*/{

        $GLOBALS['console']->addError ("delete_folder('$folderName',true);",
                                       "Location '$directory' doesn't exist.");
        return false;//end error handling by returning false
    }

  }

}
?>
