/*
    Function that generates links for the image roll over on the index page.
*/
function eventLink (eventId){
    window.location.href = '?route=events-index#event-'+eventId;
}
/*
    function takes values in a form and makes them into an object to be sent through ajax.
*/

function formToArray (formId){
    formData =  $("#"+formId).serializeArray();
    output = {};
    for (i = 0; i < formData.length; i++){
        output[ formData[i]["name"] ] = formData[i]["value"]
    }
    return output;
}


/*
    Helper function - short for get element by id
*/
function gbid(id){
    return document.getElementById(id);
}

function toastErrors (resultArray,fieldName){
    console.log(resultArray);
    for (i = 0; i <resultArray.length; i++){
        console.log(fieldName+": "+resultArray[i]);
        Materialize.toast(fieldName+": "+resultArray[i], 4000);
    }
}


function castBool (num){
    returnval = -1;
    if (num == 1 || num == "1"){
        returnval = true;
    }
    if (num == 0 || num == "0"){
        returnval = false;
    }
    // console.log("num="+num);
    // console.log("returnval="+returnval);
    return returnval;
}
function castBoolToInt (boolVal){
    if (boolVal == true){
        return 1;
    }
    if (boolVal == false){
        return 0;
    }
    return -1;
}

function openModal (modalId){
    Materialize.updateTextFields();
    document.querySelector(".modalLoading").innerHTML = `
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                </div>
            </div>
        </div>
    `;
    $('#'+modalId).modal('open');
    $( "#"+modalId ).find( ".modal-footer" ).find(".modalSend").show();
    $("#"+modalId ).find( ".modal-footer" ).find(".modalLoading").hide();
}

function modalSend (modalId){
    $( "#"+modalId ).find( ".modal-footer" ).find(".modalSend").hide();
    $( "#"+modalId ).find( ".modal-footer" ).find(".modalLoading").show();
}

function modalComplete (modalId){
    $( "#"+modalId ).find( ".modal-footer" ).find(".modalSend").hide();
    $( "#"+modalId ).find( ".modal-footer" ).find(".modalLoading").hide();
}

function inputGetVal (formId,inputName){
    return $( "#"+formId).find(".input-field").find("input[name="+inputName+"]").val();
}

function inputGetSelect (formId,inputName){
    return $( "#"+formId).find(".input-field").find("select[name="+inputName+"]").val();
}

function inputSetSelect (formId,inputName,value){
    //$( "#"+formId).find(".input-field").find("select[name="+inputName+"]").material_select();
    //$('select').material_select();
    //$( "#"+formId).find(".input-field").find("select[name="+inputName+"]").val(value);

    sbox = $( "#"+formId).find(".input-field").find("select[name="+inputName+"]");
    sbox.val(value);
    console.log("val = "+ value);
    sbox.material_select();
    //$('#my-select').
//$("#my-select").material_select();
}


function inputSetTextarea (formId,inputName, value){
    return $( "#"+formId).find(".input-field").find("textarea[name="+inputName+"]").val(value);
}

function inputSetVal (formId,inputName,value){
    $( "#"+formId).find(".input-field").find("input[name="+inputName+"]").val(value);
}

// function inputSetChecked (formId,inputName,value){
//     $( "#"+formId).find(".input-field").find("input[name="+inputName+"]").val(value);
// }
