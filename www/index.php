<?php
use Illuminate\Validation;
use Illuminate\Filesystem;
use Illuminate\Translation;
use Illuminate\Database\Capsule\Manager as Capsule;
use Handlebars\Handlebars;
//laravel import of validator provided from https://stackoverflow.com/a/41956830
    require_once ( './../vendor/autoload.php'); //include composer libs
    require_once ('./../app.f.php'); //include app
    require_once ('./../responce.f.php'); //include responce class
    include ('./../framework/console.php');
    /* set up request vars*/
    $req = [];
    $req ['indexFolder'] = str_replace("/index.php","", $_SERVER['SCRIPT_NAME']);
    $req ['URI'] = str_replace($req["indexFolder"],"", $_SERVER['REQUEST_URI']);
    $req ['reqMode'] = $_SERVER['REQUEST_METHOD'];
    $req ['body'] = $_POST;
    $req ['query'] = $_GET;
    /* set up responce obj */
    $res = new res();
    /* start app */
    $app = new app([
        'filePath' => './../.env'
    ]);
    /* import laravel validator */
    $app->use('filesystem',new Filesystem\Filesystem());
    $app->use(
            'fileLoader',
             new Translation\FileLoader($app->getLib('filesystem'), '')
         );
    $app->use(
        'translator',
        new Translation\Translator($app->getLib('fileLoader'), 'en_US')
    );
    $app->use(
        'factory',
        new Validation\Factory($app->getLib('translator'))
    );
    /* boot laravels Eloquent ORM */
    $app->use('capsule', new Capsule());
    $app->getLib('capsule')->addConnection($app->getEnv("mySQLdev"));
    $app->getLib('capsule')->bootEloquent();
    /* add handlebars and set as render engine*/
    $app->use('engine',new Mustache_Engine());
    $app->useRenderEngine(
        $res,
        $app->getLib('engine')
    );
    /* use the console */
    $app->use('console', new Console());

    //get routs
    require_once ('./../routes.r.php');
?>
