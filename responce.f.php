<?php
/**
 * [res description]
 * handles all the responce
 */
class res {
    public $engine = null; //where the render engine is stored
    public function __construct(){}
    /**
     * [send description]
     * used for sedign symple text to the cloent
     * @param  [string] $str [description] string to be returned
     * @echo [string] echos string on to page
     * @return [null]      [description]
     */
    public function send ($str){
        echo $str;
        die;
    }
    /**
     * [render description]
     * renders a view using the render engine
     * @param  [string] $file [string from view file]
     * @param  [array]]  $data [data to be used on the view]
     * @echo [string] echos the rendered view and sends it to client
     * @return [null]       [description]
     */
    public function render ($file,$data=[]){
        echo $this->engine->render($file,$data);
        die;
    }
    /**
     * [json description]
     * echos json data on the page so that it is returned as json to client
     * @param  [type] $array [description]
     * @return [type]        [description]
     */
    public function json($array){
        echo json_encode($array);
        die;
    }
}
?>
